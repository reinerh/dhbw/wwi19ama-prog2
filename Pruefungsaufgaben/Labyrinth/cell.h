#ifndef CELL_H
#define CELL_H

/**
 * Hilfsklasse fuer die Zellen des Labyrinth-Spielfelds.
 * Wird in Playfield intern genutzt.
 *
 * Ein Cell-Objekt kann man fragen, ob es blockierend ist, ob der Spieler oder der Schatz darin sind und ob es
 * leer ist. Außerdem kann man einen Spieler oder Schatz darauf setzen und wieder entfernen.
 *
 * Die Klasse hat zwei Konstruktoren: Der Standardkonstruktor erzeugt eine leere Zelle.
 * Der zweite Konstruktor erwartet ein char, so dass eine Zelle implizit aus einem Zeichen konstruiert werden kann.
 * Dies wird in Playfield beim Einlesen des Spielfelds gemacht.
 *
 * Ausserdem ist noch der Cast-Operator ueberladen, so dass ein Cell-Objekt auch implizit wieder in ein char
 * umgewandelt wird, wenn das benoetigt wird. Dies wird in Playfield::str() genutzt.
 */
class Cell
{
public:
    // Konstruktoren
    Cell();
    Cell(char c);

    // Funktionen zur Abfrage des Zellen-Zustands
    bool isBlocking();
    bool playerPresent();
    bool treasurePresent();
    bool empty();

    // Funktionen zum Aendern des Zellen-Zustands
    bool addPlayer();
    void removePlayer();
    bool addTreasure();
    void removeTreasure();

    // Cast-Operator, um eine Implizite Konvertierung nach Char zu ermoeglichen.
    operator char() const;
    
private:
    bool isWall;
    bool hasTreasure;
    bool hasPlayer;
};

#endif
