#include "playfield.h"

#include <iostream>
using namespace std;

/* Demo zur Benutzung der Playfield-Klasse.
 *
 * Auf dem 6. Test-Spielfeld findet man den Schatz, indem man nach rechts, oben und dann links geht.
 * Hier ist ein Beispielprogramm, das diese Schritte macht und dabei die Funktionen der Klasse Playfield nutzt.
 * Es gibt drei While-Schleifen, die jeweils so lange eine der Richtungen ausprobieren, bis es nicht mehr geht.
 * Am Ende wird dann einmal die Popsitionshistorie ausgegeben und geprüft, ob der Schatz auch wirklich gefunden wurde.
 */

int main()
{
    Playfield p6("tests/playfield06.txt");

    cout << p6 << endl;

    while (p6.canMoveEast())
    {
        p6.moveEast();
    }
    while (p6.canMoveNorth())
    {
        p6.moveNorth();
    }
    while (p6.canMoveWest())
    {
        p6.moveWest();
    }

    cout << p6.getPositionHistory();

    if (p6.treasureFound())
    {
        cout << "Gefunden!" << endl;
    }

    return 0;
}