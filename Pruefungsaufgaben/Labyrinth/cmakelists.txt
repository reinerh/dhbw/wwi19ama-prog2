# Build-Konfigurationen fuer die Labyrinth-Aufgabe

cmake_minimum_required(VERSION 3.10)

# Projektname
project(Labyrinth)

# Angabe des C++-Standards
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS NO)

# Name der Exe-Datei und der zugehörigen Quellen
add_executable(testPlayfieldOutput testPlayfieldOutput.cpp playfield.cpp cell.cpp)
add_executable(demo demo.cpp playfield.cpp cell.cpp)