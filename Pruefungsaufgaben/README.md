# Prüfungsaufgaben

Hier werden die Aufgaben für die Portfolio-Prüfung beschrieben.
Die Aufgaben können wieder in Gruppen zu max. 3 Personen bearbeitet werden.
Es muss am Ende der Code inkl. übersichtlicher Dokumentation/Kommentierung abegeben werden.

Die Dokumentation muss kein ausführliches Dokument sein, jedoch sollte für jede selbstgeschriebene Funktion/Klasse eine kurze Beschreibung vorliegen, was der Zweck ist, wie sie verwendet wird und was die zentralen Ideen sind. Soweit diese Dinge offensichtlich sind, weil die Funktion z.B. sehr kurz ist und/oder einen sprechenden Namen hat, können diese Dinge auch weggelassen werden.

Die Abgabe sollte bis zum Ende des Semesters erfolgen, vorher gibt es mit jedem Teilnehmer bzw. jeder Gruppe  eine kurze Besprechung, in der Sie Ihre Ideen bzw. die Vorgehensweise vorstellen sollen. Dazu müssen die Aufgaben noch nicht vollständig bearbeitet sein, ein Konzept/Ansatz muss natürlich vorhanden sein. **Insbesondere bei Gruppen muss jeder Teilnehmer etwas beizutragen haben**.

#### Zusammenfassung:

- **mündliche Kontrolle:**
  - Einzeln oder in Kleingruppen per Discort / Repl.It
  - Termine nach Absprache ab dem 27. April
- **Abgabe:**
  - bis zum 8.5.2020 per Mail oder Disord


## Aufgabe: Heapsort

Im Ordner `Heap-Vorgabe` liegt eine Beispiel-Implementierung eines Heaps, der Zahlen speichert.
Es gibt die folgenden Dateien:

- `heap.h`: Header mit der Definition der Heap-Klasse
- `heap.cpp`: Quelldatei mit der Implementierung zu `heap.h`
- `test.cpp`: Tests zur Heap-Implementierung

**Aufgabenstellung:**

1. Implementieren Sie den Algorithmus "HeapSort" mittels der vorgegebenen Datenstruktur.
  - *Möglichkeit 1:* Schreiben Sie eine freie Funktion, die als Argument einen
    `vector<int>` erwartet, und die intern einen Heap erzeugt und benutzt,
    um den Vektor zu sortieren.
  - *Möglichkeit 2:* Schreiben Sie HeapSort als Member-Funktion von `Heap`.
    Diese Funktion würde dann eine sortierte Version der Elemente im Heap
    zurückliefern.
  - **Anmerkung:** Sie müssen die Klasse `Heap` bzw. die bisherigen Funktionen 
    dafür nicht verändern, sondern nur auf die richtige Art verwenden.
    Testen Sie Ihre HeapSort-Implementierung, indem Sie 
    verschiedene Listen damit sortieren.
2. Optimieren Sie Ihre Implementierung.
  - Vermeiden Sie unnötige Kopiervorgänge der Elemente, indem Sie möglichst
    direkt mit dem Element-Array arbeiten. Dazu kann es sinnvoll sein,
    die bisherigen Member der Klasse `Heap` zu verändern und dabei auch
    mit Pointern oder Referenzen zu arbeiten.
  - Vermeiden Sie auch doppelten Code, indem Sie, wo möglich, direkt die
    Member-Funktionen von Heap benutzen. Falls Sie ähnliche Funktionalität
    bereitstellen, wie sie ein vorhandener Member bietet, überlegen Sie sich,
    ob man die Funktionen vereinheitlichen oder gemeinsam verwendete
    Hilfsfunktionen erstellen kann.

**Hinweis:**
Die Tests in `test.cpp` müssen mit Ihrer geänderten Klasse `heap` weiterhin funktionieren!


## Aufgabe: Heap als Listen-Struktur für Datenelemente

Wir haben in der Vorlesung besprochen, dass Heaps die zugrundeliegende Struktur für eine Prioritätswarteschlagen sein können.
Ein einfaches Beispiel für eine solche Prioritätswarteschlange ist eine ToDo-Liste mit Einträgen,
die jeweils eine Beschreibung und ein Fälligkeitsdatum oder eine Priorität haben.

**Aufgabenstellung:**
1. Definieren Sie einen einfachen Element-Datentyp für Datensätze wie z.B. Aufgaben mit
   Terminen oder Prioritäten.
   Sie können sich auch ein anderes Beispiel für eine Prioritätswarteschlange überlegen.
   Wichtig dabei ist: Die Elemente müssen in sinnvoller Weise miteinander vergleichbar sein.
2. Verändern Sie die Heap-Implementierung so, dass sie mit Ihrem Element-Datentyp funktioniert.

**Hinweis:**
Die Tests in `test.cpp` müssen mit Ihrer geänderten Klasse `heap` weiterhin funktionieren! Passen Sie sie ggf. an, so dass auch hier der neue Datentyp verwendet wird.

## Aufgabe: Wegsuche in einem Labyrinth

Im Ordner `Labyrinth` liegt eine Klasse `Playfield` mit zwei Beispiel-Code-Dateien.
Diese Klasse erwartet bei ihrer Konstruktion eine Textdatei, in der ein Labyrinth
mit Wänden, einer Startposition und einem Schatz gegeben ist. Die Startposition ist mit 'P' markiert ("Player"), der Schatz mit 'T' ("Treasure") und die Wände sind Rauten ('#').
Im Unterordner `tests` sind einige Beispiel-Spielfelder vorgegeben.
Die Klasse `Labyrinth` liest solch eine Textdatei ein und bietet verschiedene Funktionen, mit denen man den Spieler im Labyrinth bewegen kann. Dabei protokolliert sie die Bewegungen, sie sind mit der Funktion `Playfield::getPositionHistory()` abrufbar.

**Aufgabenstellung:**
1. Implementieren Sie eine Wegsuche, die im Labyrinth von 'P' nach 'T' findet,
   indem sie in jedem Schritt eine der Funktionen `moveNorth()`, `moveEast()` etc. aufruft.
   Am Ende soll ein durchgehender Weg von 'P' nach 'T' in der History stehen.
   Ihre Lösung muss mit den Dateien `playfield01.txt` bis `playfield06.txt` funktionieren.
2. Verändern Sie die vorgegebene Klasse, so dass es auch mit `playfield07.txt` 
   funktioniert. Dafür müssen Sie die Nachbarschaftsbeziehungen, die in den
   `move`-Funktionen fest eingebaut sind, neu definieren.
   

## Aufgabe für Fortgeschrittene:
Erweitern Sie die Klasse `Playfield`, so dass sie weitere, interessantere
Varianten des Labyrinths ermöglicht. Es gibt verschiedene Möglichkeiten,
Sie dürfen sich auch selbst etwas ausdenken.

- Verändern Sie die Klasse `Playfield`, so dass das Spielfeld die Oberfläche eines
  Würfels mit 6 Seiten ist.
  1. Die Klasse könnte dann anstelle eines Spielfelds sechs quadratische Spielfelder
     einlesen, eines für jede Seite des Würfels.
  2. Die Nachbarschaftsbeziehungen werden dadurch erheblich komplizierter und müssen
     angepasst werden.
- Verändern Sie Ihren Algorithmus, so dass er auch mit einem Labyrinth zurechtkommt,
  in dem es größere freie Flächen gibt.
- Haben Sie eigene Ideen?



# Allgemeiner Hinweis zu den Aufgaben:

Die Aufgaben sind durchnummeriert, weil es mir sinnvoll erscheint, ungefähr in diesen Teilschritten vorzugehen. Sie müssen aber nicht unbedingt zu jeder Teilaufgabe auch eine
Abgabe einreichen. Wenn eine Abgabe mehrere Teilaufgaben löst, akzeptiere ich das auch.
Wichtig ist aber weiterhin die Dokumentation: Erklären Sie möglichst genau im Code,
was wozu gedacht ist und warum Sie diese Lösung gewählt haben. Und achten Sie bitte
auf eine gute Lesbarkeit des Codes (Einrückungen, Variablenbenennung etc.).
