#include"heap.h"

#include<algorithm>
#include<stdexcept>
using namespace std;

void Heap::add(int x)
{
    elements.push_back(x);
    bubbleUp();
}

void Heap::removeRoot()
{
    // Sicherheitscheck: Aus leerem Heap darf nichts entfernt werden.
    if (elements.empty()) { return; }

    // Wurzel mit letztem Element vertauschen, alte Wurzel loeschen und neue Wurzel einsinken lassen.
    std::swap(elements.front(), elements.back());
    elements.pop_back();
    bubbleDown();
}

int Heap::getRoot() const
{
    // Fehlerbehandlung: Bei leerem Heap werfen wir eine Exception.
    //                   Dies terminiert die Funktion und ggf. das Programm (s.u.).
    if (elements.empty()) { throw out_of_range(
        "Kann keine Wurzel eines leeren Heaps liefern."); }

    // Die Wurzel zurueckliefern.
    return elements[0];
}

size_t Heap::size() const
{
    return elements.size();
}

void Heap::bubbleUp()
{
    // Sonderfall: Bei hoechstens einem Element ist nichts zu tun.
    if (size() <= 1) { return; }

    // bubbleUp() wird immer nach dem Einfuegen eines neuen Elements ausgefuehrt.
    // Dieses neue Element steht also zu Anfang am Ende der Element-Liste.
    // Wir merken uns diese Position und aktualisieren sie unten nach jeder Vertauschung.
    size_t newElementPos = size() - 1;

    // Solange das neue Element kleiner ist als sein Elternelement, werden die beiden vertauscht.
    // Sonderfall: Wenn das neue Element ganz oben steht, brechen wir auch ab.
    while (newElementPos > 0 &&
           elements[newElementPos] < elements[parentPos(newElementPos)])
    {
        size_t newParentPos = parentPos(newElementPos);
        swap(elements[newParentPos],elements[newElementPos]);
        newElementPos = newParentPos;
    }
}

void Heap::bubbleDown()
{
    // Sonderfall: Bei hoechstens einem Element ist nichts zu tun.
    if (size() <= 1) { return; }
    
    // Die Position des zu bewegenden Elements ist am Anfang 0.
    size_t elementPos = 0;
    
    // Die Position wandert nach unten, bis das kleinere Kind
    // entweder nicht mehr zum Baum gehoert, oder bis es groesser ist,
    // als das bewegte Element.
    while (childPos(elementPos) < size() &&
           elements[elementPos] > elements[childPos(elementPos)])
    {
        size_t newChildPos = childPos(elementPos);
        swap(elements[elementPos],elements[newChildPos]);
        elementPos = newChildPos;
    }
}

size_t Heap::parentPos(size_t n)
{
    if (n == 0) { throw out_of_range("Parent-Position zur Wurzel verlangt."); }
    return (n - 1) / 2;
}

size_t Heap::childPos(size_t n)
{
    // Beide Kind-Positionen berechnen
    size_t const leftPos = 2 * n + 1;
    size_t const rightPos = leftPos + 1;

    // Fehlerfall: Wenn das rechte Kind nicht im Baum ist,
    //             Position des linken Kindes liefern.
    //             Ob diese zum Baum gehoert, muss der Aufrufer selbst pruefen.
    if (rightPos >= size()) { return leftPos; }
    
    // Vergleich der beiden Elemente und Rueckgabe der Position des kleineren.
    return elements[leftPos] < elements[rightPos] ? leftPos : rightPos;
}


/* Erlaeuterungen zu den Exceptions in getRoot() und getParentPos():
 *
 * Die Anweisung 'throw out_of_range(...)' erzeugt eine sogenannte Exception.
 * Dies ist ein Weg, die laufende Funktion im Fehlerfallzu beenden, dabei gibt
 * man eine Fehlermeldung als String mit an.
 *
 * Exceptions koennen ausserhalb der Funktion mit Hilfe des Schlüsselworts 'catch'
 * "gefangen" werden. Dies erlaubt es, den Fehler zu behandeln und ggf. mit dem
 * Programm fortzufahren. Diese Thematik wird hier jedoch nicht weiter behandelt.
 * Wird eine Exception nicht gefangen, so endet das gesamte Programm mit der
 * angegebenen Fehlermeldung.
 *
 * Die Exception dient hier zu Demonstrationszwecken, fuer die eigentliche Aufgabe
 * ist sie nicht von Bedeutung. Wie in C++ ueblich, sind Exceptions optional, die
 * Klasse std::vector macht z.B. keine Bereichspruefung. Aufgrund des
 * kontrollierten Absturzes mit einer Fehlermeldung ist ein Fehler aber ggf. besser
 * zu erkennen als ohne die Exception.
 *
 * Wuerde man die Pruefung in Heap::getRoot() einfach weglassen und bedingungslos
 * 'elements[0]' liefern, so waere dies "undefiniertes Verhalten". Es gaebe keine
 * Garantien dafuer, was in so einem Fall passiert. Wenn 'elements[0]' einen
 * Phantasiewert lieferte, waere das genau so in Ordnung wie ein Absturz. Es laege
 * dann in der Verantwortung des aufrufenden Codes, dass Heap::getRoot() nur
 * aufgerufen wird, wenn der Heap nicht leer ist.
 * Der Compiler darf bei der Optimierung sogar annehmen, dass so ein Fall nie
 * eintritt. Manchmal werden dadurch grosse Teile des Codes wegoptimiert.
 */

