#include "heap.h"

/* Tests fuer die Heap-Implementierung.
 *
 * In dieser Datei gibt es eine Reihe von Testfaellen, die verschiedene Aspekte eines Heaps pruefen.
 * Jeder Test steht in einer eigenen Funktion, diese werden alle aus der main()-Funktion aufgerufen.
 *
 * Die Beschreibung steht jeweils bei den einzelnen Funktionen, das Grundgeruest ist immer das gleiche:
 * Jede Funktion baut einen Heap auf, benutzt dann Teile der Heap-API und prueft anschliessend, ob der Heap
 * sich wie erwartet verhaelt. Die Namen der Testfunktionen sind relativ lang, sollen dafuer aber jeweils grob den
 * Testfall beschreiben.
 *
 * Jede der Funktionen liefert die Anzahl der Fehlschlaege zurueck, die bei ihrer Abarbeitung aufgetreten sind.
 * I.D.R. ist dieser Wert 0 oder 1. Groessere Werte sollten nur in Ausnahmefaellen vorkommen, da man normalerweise
 * nicht mehrere Testfaelle in eine Funktion schreiben sollte. Die main()-Funktion zaehlt die Anzahl der Fehlschlaege.
 *
 * Anmerkung: Die Tests werden hier von Hand implementiert, um die Vorgehensweise zu demonstrieren.
 *            In der Praxis werden oft groessere Test-Frameworks wie z.B. "CPPUnit" oder "Catch" benutzt.
 *            Solche Frameworks koennen einem viel Test-Arbeit abnehmen, indem sie z.B. einfachere Pruefungen
 *            ermoeglichen oder die Ausgabe der Fehlermeldungen automatisch um korrekte Funktionsnamen,
 *            Code-Zeilen etc. ergaenzen.
 *            Einige Entwickler ziehen es aber auch vor, Tests generell von Hand zu implementieren,
 *            weil die Test-Umgebung der Produktiv-Umgebung aehnlicher ist.
 */

#include <iostream>
using namespace std;

// Deklarationen der Test-Funktionen (Implementierung s.u.)

/// Fuegt zwei Elemente zu einem Heap hinzu, erst ein kleineres, dann ein groesseres.
/// Anschliessend soll das kleinere Element in der Wurzel stehen.
int test_addSmallThenLargeElement();

/// Fuegt zwei Elemente zu einem Heap hinzu, erst ein groesseres, dann ein kleineres.
/// Anschliessend soll das kleinere Element in der Wurzel stehen.
int test_addLargeThenSmallElement();

/// Fuegt mehrere Elemente zu einem Heap hinzu und entfernt diese nacheinander.
/// Die Elemente muessen in aufsteigender Reihenfolge entfernt werden.
int test_addAndRemoveSeveralElements();

/// Erzeugt einen Heap mit zwei Elementen und loescht dann die Wurzel.
/// Anschliessend soll das groessere der beiden Elemente in der Wurzel stehen.
int test_deleteFromHeapWithTwoElements();

/// Ruft remove auf einem leeren Heap auf.
/// Die Funktion darf nicht abstuerzen.
int test_removeFromEmptyHeap();

/// Fuegt ein Element zum Heap hinzu.
/// Die Groesse muss danach 1 sein.
int test_sizeAfterAdd();

/// Entfernt die Wurzel aus einem einelementigen Heap.
/// Die Groesse muss danach 0 sein.
int test_sizeAfterRemove();

int main()
{
    int failureCount = 0;

    failureCount += test_addSmallThenLargeElement();
    failureCount += test_addLargeThenSmallElement();
    failureCount += test_addAndRemoveSeveralElements();
    failureCount += test_deleteFromHeapWithTwoElements();
    failureCount += test_removeFromEmptyHeap();
    failureCount += test_sizeAfterAdd();
    failureCount += test_sizeAfterRemove();

    if (failureCount == 0)
    {
        cout << "Alle Tests gruen :-)" << endl;
    }
    else
    {
        cout << endl;
        cout << "Alle Tests ohne Absturz durchgelaufen," << endl;
        cout << "aber " << failureCount << " Tests fehlgeschlagen." << endl;
    }

    return failureCount;
}

int test_addSmallThenLargeElement()
{
    // Test-Voraussetzungen herstellen:
    Heap h;

    // Zu pruefende Operation durchfuehren:
    h.add(10);
    h.add(20);

    // Pruefung des Ergebnisses:
    int r = h.getRoot();
    if (r != 10)
    {
        cout << "test_addSmallThenLargeElement():" << endl;
        cout << "Fehler: Die Wurzel ist nicht das kleinere der beiden eingefuegten Elemente." << endl;
        cout << "Erwarteter Wert: 10, tatsaechlicher Wert: " << r << endl << endl;
        return 1;
    }

    return 0;
}

int test_addLargeThenSmallElement()
{
    // Test-Voraussetzungen herstellen:
    Heap h;

    // Zu pruefende Operation durchfuehren:
    h.add(20);
    h.add(10);

    // Pruefung des Ergebnisses:
    int r = h.getRoot();
    if (r != 10)
    {
        cout << "test_addLargeThenSmallElement():" << endl;
        cout << "Fehler: Die Wurzel ist nicht das kleinere der beiden eingefuegten Elemente." << endl;
        cout << "Erwarteter Wert: 10, tatsaechlicher Wert: " << r << endl << endl;
        return 1;
    }

    return 0;
}

int test_addAndRemoveSeveralElements()
{
    // Test-Voraussetzungen herstellen:
    Heap h;

    h.add(2);
    h.add(10);
    h.add(22);
    h.add(-3);
    h.add(38);

    // Zu pruefende Operation durchfuehren:
    vector<int> elems;
    size_t heapsize = h.size();
    for (int i=0; i<heapsize; i++)
    {
        elems.push_back(h.getRoot());
        h.removeRoot();
    }

    // Pruefung des Ergebnisses:
    for (int i=0; i<elems.size() - 1; i++) {
        if (elems[i] > elems[i+1])
        {
            cout << "test_addAndRemoveSeveralElements():" << endl;
            cout << "Fehler: Die Elemente des Ergebnis-Vektors sind nicht aufsteigend sortiert." << endl;
            cout << "Reihenfolge bis zum Fehler: ";
            for (int el:elems)
            {
                cout << el << " ";
            }
            cout << endl;
            return 1;
        }
    }

    return 0;
}

int test_deleteFromHeapWithTwoElements()
{
    // Test-Voraussetzungen herstellen:
    Heap h;
    h.add(20);
    h.add(10);

     // Zu pruefende Operation durchfuehren:
    h.removeRoot();

    // Pruefung des Ergebnisses:
    int r = h.getRoot();
    if (r != 20)
    {
        cout << "test_deleteFromHeapWithTwoElements():" << endl;
        cout << "Fehler: Die Wurzel ist nicht das groessere der beiden eingefuegten Elemente." << endl;
        cout << "Erwarteter Wert: 20, tatsaechlicher Wert: " << r << endl << endl;
        return 1;
    }

    return 0;
}

int test_removeFromEmptyHeap()
{
    // Test-Voraussetzungen herstellen:
    Heap h;

    // Zu pruefende Operation durchfuehren:
    h.removeRoot();

    return 0;
}

int test_sizeAfterAdd()
{
    // Test-Voraussetzungen herstellen:
    Heap h;

    // Zu pruefende Operation durchfuehren:
    h.add(10);

    // Pruefung des Ergebnisses:
    int s = h.size();
    if (s != 1)
    {
        cout << "test_sizeAfterAdd():" << endl;
        cout << "Fehler: Die Groesse des Heaps nach dem Einfuegen eines Elements stimmt nicht." << endl;
        cout << "Erwarteter Wert: 1, tatsaechlicher Wert: " << s << endl << endl;
        return 1;
    }

    return 0;
}

int test_sizeAfterRemove()
{
    // Test-Voraussetzungen herstellen:
    Heap h;
    h.add(10);

    // Zu pruefende Operation durchfuehren:
    h.removeRoot();

    // Pruefung des Ergebnisses:
    int s = h.size();
    if (s != 0)
    {
        cout << "test_sizeAfterRemove():" << endl;
        cout << "Fehler: Die Groesse des Heaps nach dem Loeschen eines Elements stimmt nicht." << endl;
        cout << "Erwarteter Wert: 0, tatsaechlicher Wert: " << s << endl << endl;
        return 1;
    }

    return 0;
}
