# Build-Konfigurationen fuer die Heap-Implementierung in diesem Ordner.

cmake_minimum_required(VERSION 3.10)

# Projektname
project(Heap)

# Angabe des C++-Standards
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS NO)

# Name der Exe-Datei und der zugehörigen Quellen
add_executable(test test.cpp heap.cpp)
