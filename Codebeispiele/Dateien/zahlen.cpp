/**
 * Beispiel zum Lesen und Schreiben von Dateien.
 *
 * Dieses Beispiel demostriert, wie man Dateien zum Lesen/Schreiben
 * öffnet und verwendet. Das Beispiel liest Zahlen aus einer Datei und
 * schreibt für jede gelesene Zahl die entsprechende Anzahl Sterne in
 * eine andere Datei.
 */

// Für ifstream und ofstream (s.u.) wird der Header
// <fstream> gebraucht
#include<fstream>
using namespace std;

int main()
{
    // Wir erzeugen zwei Datei-Stream-Objekte,
    // eines zum Lesen, eines zum Schreiben
	ifstream infile("zahlen.txt");
	ofstream outfile("sterne.txt");
	
    // Variable für die jeweils eingelesenen Zahlen.
	int zahl;
    
    /* Schleife, die nacheinander jede Zahl aus infile liest.
     * infile >> zahl liest eine Zahl und wird true, falls es
     * geklappt hat.
     * D.h. diese Schleife läuft so lange, wie erfolgreich
     * Zahlen gelesen werden können.
     */
	while (infile >> zahl)
	{
        // zahl Sterne in die Ausgabedatei schreiben.
		for (int x=0; x<zahl; x++)
		{
			outfile << '*';
		}
        // Nach jeder Zahl einen Zeilenumbruch schreiben.
		outfile << endl;
	}
    
    // Beide Dateien schließen.
    // Besonders bei geschriebenen Dateien wichtig!
	infile.close();
	outfile.close();
	
	return 0;
}
