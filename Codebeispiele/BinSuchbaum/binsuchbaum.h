#ifndef BINSUCHBAUM_H
#define BINSUCHBAUM_H

#include<iostream>
#include<string>

/* Vorausdeklaration der Klasse Knoten, damit wir
   einen Pointer auf einen Knoten deklarieren k�nnen.
 */ 
class Knoten;

class BinSuchBaum
{
	Knoten * root;            // Die Wurzel des Baumes
	
	
public:
	/** Konstruktor: Erzeugt einen Baum mit Dummy-Wurzel **/
	BinSuchBaum();
	
	/** Gibt an, ob der Baum leer ist. **/
	bool empty();
	
	/** Fuegt ein neues Datenelement hinzu. **/
	void add(int newdata);
	
	/** Gibt den gesamten Baum als String zurueck. **/
	std::string str();
		
	/** Loescht den Knoten mit dem angegebenen Schl�ssel **/
	void erase(int requestedKey);

// Private Hilfsfunktionen
private:	
	/** Konvertierender Konstruktor:
	    Macht aus einem Knoten einen Baum.
	    Anmerkung: Wird nur intern verwendet,
	    deshalb privat.
	 **/
	BinSuchBaum(Knoten * k);
	
	/** Vertauscht die beiden angegebenen Knoten. **/
	void swap(Knoten * k1, Knoten * k2);
	
	/** Liefert den In-Order-Nachfolger des
	    gegebenen Knotens in dessen Teilbaum. **/
	Knoten * findSuccessor(Knoten * k);
};

/** Ueberladung des Stream-Ausgabe-Operators **/
std::ostream & operator<<(std::ostream & left, BinSuchBaum & right);

#endif
