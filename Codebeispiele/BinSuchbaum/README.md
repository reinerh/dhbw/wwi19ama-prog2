# Beispiel-Implementierung eines binären Suchbaums

Hier findet sich eine kurze Implementierung binärer Suchbäume mit Funktionen zum Hinzufügen von Elementen und Ausgeben des gesamten Baumes. Im Gegensatz zur Version aus der Vorlesung ist der Baum hier in zwei verschiedene Klassen unterteilt:  `Knoten` ähnelt der Struktur aus der Vorlesung, zusätzlich gibt es eine Klasse `BinsSuchBaum`. Diese kapselt einen Knoten-Pointer und bietet die öffentliche Schnittstelle an.

Einige Funktionen eines Suchbaums sind in `Knoten` implementiert, weil sie dort direkter implementiert werden können.

Zum Beispiel gilt das für die Funktion `add()`. Diese existiert in `BinSuchBaum`, delegiert aber direkt an die entsprechende Funktion in `Knoten`. Diese ist rekursiv implementiert, was von `BinSuchBaum` aus schwierig wäre.

Die Funktion `str()` dagegen ist in `BinSuchBaum` ganz anders als in Knoten. Sie gibt einen gesamten Baum in In-Order-Darstellung als String zurück, während die `Knoten`-Variante nur den Wert eines Knotens liefert.
Diese Funktion ist auch rekursiv, da sie in `Knoten` aber anders ist, muss sie hier in BinSuchBaum implementiert werden. Eine Alternative dazu wäre gewesen, den In-Order-Durchlauf unter anderem Namen in `Knoten` zu implementieren.

Andere (theoretisch denkbare) Funktionen, wie z.B. eine Lösch-Funktion oder eine AVL-Rotation müssten auf jeden Fall ih `BinSuchBaum` bleiben, weil sie sich mit mehreren Knoten gleichzeitig beschäftigen.
