# Schere-Stein-Papier

In diesem Verzeichnis finden sich die Beispielimplementierungen zu Schere-Stein-Papier aus den Jupyter-Notebooks zu Structs und Klassen.

## Anweisungen zum Compilieren

Die Implementierungen sind jeweils in einer Datei, die wie gewohnt auf der Konsole z.B. mittels `g++` compiliert werden kann.

### Auf der Konsole

Hier in der Jupyter-Umgebung muss explizit mit angegeben werden, dass eine hinreichend neue Version von C++ verwendet werden soll.
Das geht, indem man z.B. `-std=c++17` mit in die Compiler-Zeile mit aufnimmt.
Die folgende Zeile compiliert z.B. die erste SSP-Version:

```
g++ -std=c++17 -o ssp01 ssp01.cpp 
```

### Mittels CMake

Es gibt auch eine `cmakelists.txt`, die
ein CMake-Projekt konfiguriert.
Dabei gibt es ein *Target* für jede SSP-Version.
In Entwicklungsumgebungen mit CMake-Unterstützung sollte es dadurch möglich sein, gezielt einzelne der Quelldateien zu bauen.

#### Alles per CMake auf der Konsole bauen:
Wer auf der Konsole alles auf einmal bauen will, gibt einfach folgendes ein:

```cmake --build .```

#### Einzelne Dateien mit CMake bauen:

Wer CMake so nutzen will, wie es eigentlich gedacht ist, lässt sich mit folgendem Befehl ein *Build-System* erzeugen:

```cmake .```

Dadurch entsteht eine Datei namens `Makefile`, die folgendermaßen genutzt werden kann, um z.B. Version 1 zu bauen:

```make ssp01```

Dies wiederum erzeugt die ausführbare Datei `ssp01` oder `ssp01.exe`.

Der erste Schritt (`cmake .`) muss nur ausgeführt werden, wenn Dateien hinzugekommen sind oder sich etwas an der CMake-Konfiguration geändert hat.

