#include<iostream>
#include<random>
#include<string>

/* Die Version von Schere-Stein-Papier aus
 * dem Erklärungsnotebook zu Vererbung.
 * (C++Konzepte/08-Klassen-Konstruktoren.ipynb)
 */

// Vorausdeklarationen aller Funktionen und Structs,
// damit die Implementierungsreihenfolge keine Rolle spielt.
// Anmerkung: Structs müssen zuerst kommen, da ihre Größe bekannt sein muss.


/** Abstrakte Spieler-Klasse.
 * Hier wird das gesamte Interface eines Spielers definiert, aber nicht alle
 * Funktionen werden auch implementiert.
 */
class AbstractPlayer
{
    std::string name;
    int score;
    
public:
    // Konstruktor, initialisert ein Spieler-Objekt.
    // Wird genau einmal, direkt nach Konstruktion, aufgerufen.
    AbstractPlayer(std::string);
    
    // Liefert den Namen des Spielers.
    std::string get_name();
    
    // Liefert den Punktestand des Spielers.
    int get_score();
    
    // Erhöht den Punktestand des Spielers.
    void increase_score();
    
    // Fragt eine Eingabe vom Spieler ab und liefert
    // als Ergebnis 1 (Schere), 2 (Stein) oder 3 (Papier) zurück.
    virtual int get_move() = 0;
    
    // virtual bedeutet, dass diese Funktion in Subklassen überschrieben werden kann.
    // ...=0 am Ende bedeutet, dass die Funktion in Subklassen überschrieben werden muss.
    // Die Deklaration hier gibt also nur die Signatur einer Funktion vor, die von
    // Subklassen geliefert werden muss.
};


// Subklasse für menschliche Spieler.
// Überschreibt get_move(), so dass auf der Konsole nach dem Spielzug gefragt wird.
class HumanPlayer : public AbstractPlayer
{
public:
    // Konstruktor
    HumanPlayer(std::string);

    // Implementierung von get_move()
    virtual int get_move() override;
    
    // Mit override teilen wir mit, dass wir eine Funktion aus der Basisklasse überschreiben.
    // Das Schlüsselwort ist technisch nicht notwendig, aber so überprüft der Compiler, ob
    // wir tatsächlich überschreiben, oder ob wir versehentlich die Signatur geändert haben.
};

// Subklasse für KI-Spieler.
// Überschreibt get_move(), so dass ein Zufallsgenerator verwendet wird.
class AiPlayer : public AbstractPlayer
{
    // Ein zusätzliches Attribut: Die Zufallsengine, die in get_move() benutzt wird.
    std::default_random_engine engine;
    
public:
    // Konstruktor
    AiPlayer(std::string);

    // Implementierung von get_move()
    virtual int get_move() override;
};

// Erzeugt zwei Spieler und führt mehrere Runden durch,
// bis ein Spieler drei Punkte erreicht hat.
void run();

// Main-Funktion, startet das Spiel.
int main()
{
    run();
    
    return 0;
}    

AbstractPlayer::AbstractPlayer(std::string name_)
: name(name_), score(0)
{}

std::string AbstractPlayer::get_name()
{
    return name;
}

int AbstractPlayer::get_score()
{
    return score;
}

void AbstractPlayer::increase_score()
{
    score++;
}

HumanPlayer::HumanPlayer(std::string name_)
: AbstractPlayer(name_)
{}

int HumanPlayer::get_move()
{
    // Spieler zur Eingabe eines Zugs auffordern.
    std::cout << get_name() << ", Ihre moeglichen Zuege sind: \n"
              << "1: Schere, 2: Stein, 3: Papier\n\n";
    std::cout << "Bitte geben Sie Ihren Zug ein und bestätigen Sie mit ENTER:" << std::endl;
    
    // Eingabe einlesen und pruefen, wiederholen bis Erfolg.
    bool valid = false;
    std::string input;
    while (!valid)
    {
        std::cin >> input;
        valid = (input == "1" || input == "2" || input == "3");
        if (!valid)
        {
            std::cout << "\nFehler, bitte versuchen Sie es noch einmal:" << std::endl;
        }
    }
    
    // Eingabe in Zahl konvertieren und zurueckliefern.
    // input[0] ist der ASCII-Code von '1', '2' oder '3'.
    // Wir ziehen davon den ASCII-Code von '0' ab,
    // um eine Zahl 1 <= x <= 3 zu erhalten.
    return input[0] - '0';
}

AiPlayer::AiPlayer(std::string name_)
: AbstractPlayer(name_)
, engine(std::random_device()())
{}

int AiPlayer::get_move()
{
    std::uniform_int_distribution<> dis(1, 3);
    return dis(engine);
}

void run()
{
    // Spieler erzeugen.
    HumanPlayer p1("Mensch");
    AiPlayer p2("Maschine");    
    
    // Spiel so lange laufen lassen, bis einer 3 Punkte hat.
    while (p1.get_score() < 3 && p2.get_score() < 3)
    {
        // Spielzuege abfragen
        int m1 = p1.get_move();
        int m2 = p2.get_move();
        
        // Auswertung
        if (m1 == m2) // Unentschieden
        {
            std::cout << "Diese Runde endet unentschieden." << std::endl;
        }
        else if (m2 == m1+1 || m2 == m1-2) // Spieler 2 gewinnt.
        {
            std::cout << p2.get_name() << " gewinnt diese Runde." << std::endl;
            p2.increase_score();
        }
        else
        {
            std::cout << p1.get_name() << " gewinnt diese Runde." << std::endl;
            p1.increase_score();
        }
        std::cout << std::endl;
    }
    
    // Ergebnis mitteilen
    std::cout << (p1.get_score() == 3 ? p1.get_name() : p2.get_name()) << " hat gewonnen." << std::endl;
}