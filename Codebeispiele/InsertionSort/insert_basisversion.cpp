#include <iostream>
#include <string>
#include <vector>
using namespace std; 

void insertsort(vector<int> & v)
{
  // Invariante: Links von i ist in v alles sortiert.
  for (int i=0; i<v.size(); i++)
  {
    // Element an Stelle i soll so lange nach links wandern,
    // bis es größer ist, als sein linker Nachbar.
    int j=i;
    while(j > 0 && v[j] < v[j-1]) 
    {
      // Vertausche v[i] und v[i-1]
      int h = v[j];
      v[j] = v[j-1];
      v[j-1] = h;

      j--;
    }
  }

  // Schlussbedingung: Links von v.size() ist alles sortiert.
}

void print(vector<int> v);

int main() {

  vector<int> v1{3, 15, 42, 8, 38};
  
  insertsort(v1);

  print(v1);

  return 0;
}

void print(vector<int> v)
{
  for (auto el:v) // Range-Based For-Loop
  {
    cout << el << " ";
  }

  /* Zähler-Basierte Alternative
  for (int i=0; i<v.size(); i++)
  {
    auto el = v[i];
    cout << el << " ";
  }
  */

  cout << endl;
}
