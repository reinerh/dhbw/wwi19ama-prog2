#include <iostream>
#include <vector>
#include <chrono>
using namespace std::chrono;
using namespace std;

// Eine Funktion, die einen Vektor und eine Position i erwartet.
// Sie summiert die Elemente bis einschl. Stelle i auf und liefert das Ergebnis zurück.
//
// Die Funktion selbst ist eigentlich nicht von Bedeutung, sie ist recht schnell abgearbeitet und liefert nur eine Zahl.
// Dies dient dazu zu zeigen, wie viel Zeit es kosten kann, eine große Datenstruktur an eine Funktion zu geben.
int sum2i(vector<int> v, int i)
{
    int result = 0;
    while (i >= 0)
    {
        result += v[i];
        i--;
    }
    return result;
}

// Die gleiche Funktion, dieses Mal wird allerdings eine Referenz übergeben.
// D.h. es werden nicht alle Elemente kopiert, sondern nur eine Speicheradresse.
int sum2i_Ref(vector<int> & v, int i)
{
    int result = 0;
    while (i >= 0)
    {
        result += v[i];
        i--;
    }
    return result;
}

// Wir erzeugen nun einen großen Vektor und geben ihn an jede der obigen Funktionen je einmal.
// Dabei messen wir die Zeit, die es dauert, bis die Funktion abgelaufen ist.
int main()
{
    cout << "Starte Programm, Erzeuge Vektor..." << endl;
    vector<int> v1(100000000, 10);

    cout << "Rufe sum2i auf" << endl;
    auto start1 = high_resolution_clock::now();
    int r1 = sum2i(v1, 10);
    auto stop1 = high_resolution_clock::now();
    cout << "Berechnung zu Ende." << endl << endl;

    // Eine Zeile wie "auto start1 = ..." speichert die abgelaufene Zeit in einer Variable.
    // Wir machen das je einmal zu Beginn und zum Ende jeder Funktion.

    cout << "Rufe sum2i_Ref auf" << endl;
    auto start2 = high_resolution_clock::now();
    int r2 = sum2i_Ref(v1, 10);
    auto stop2 = high_resolution_clock::now();
    cout << "Berechnung zu Ende." << endl << endl;

    cout << r1 << endl;
    cout << r2 << endl;

    // Hier bestimmen wir die Zeitintervalle, die jeweils beim Ablauf der beiden Funktionen abgelaufen sind.
    // Die abgelaufenen Zeiten (in Mirkosekunden) geben wir anschließend aus.
    auto duration1 = duration_cast<microseconds>(stop1 - start1);
    auto duration2 = duration_cast<microseconds>(stop2 - start2);

    cout << "Dauer sum2i: " << duration1.count() << endl;
    cout << "Dauer sum2i_Ref: " << duration2.count() << endl;

    return 0;
}