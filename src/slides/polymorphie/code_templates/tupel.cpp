#include<iostream>
#include"tupel.h"
//#include"bruch.h"

int main() {
    tupel<int> t(3,4);
    
    std::cout << t.str() << "\n";
    std::cout << t << "\n";
    
    bruch b(4,5);
    
    std::cout << b.str() << "\n";
    std::cout << b << "\n";
    
    b.erweitern(3);
    
    std::cout << b << "\n";
    
    std::cout << ggt(3,4) << " " << ggt(2,6) << " " << ggt(10,15) << "\n";
    
    bruch c(3,4);
    bruch d(5,6);
    bruch e = c + d;
    
    std::cout << c << "+" << d << "==" << e << "\n";
    
    return 0;
}
