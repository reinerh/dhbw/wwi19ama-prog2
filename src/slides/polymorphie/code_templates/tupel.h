#ifndef TUPEL_H
#define TUPEL_H

#include<string>
#include<iostream>

template<class T>
class tupel {
protected:
    T _x,_y;
    
public:
    tupel(T x, T y) : _x{x}, _y{y} {}
    
    virtual std::string str() { return "(" + std::to_string(_x) + "," + std::to_string(_y) + ")"; }
};

template<class T>
std::ostream & operator<<(std::ostream & s, tupel<T> & t) {
    s << t.str();
    return s;
}


class bruch : public tupel<int> {

public:
    bruch(int x, int y) : tupel<int>(x,y) {}

    virtual std::string str() { return std::to_string(_x) + "/" + std::to_string(_y); }
    void erweitern(int f) { _x *= f; _y *= f; }
    
    friend bruch operator+(bruch a, bruch b);
};

int ggt(int x, int y) {
    if (x==y) return x;
    if (x<y) return ggt(y,x);
    return ggt(y,x-y);
}

int kgv(int x, int y) {
    return x * y / ggt(x,y);
}

bruch operator+(bruch a, bruch b) {
    int n = kgv(a._y,b._y);
    a.erweitern(n/a._y);
    b.erweitern(n/b._y);
    return bruch(a._x+b._x,n);
}

#endif

