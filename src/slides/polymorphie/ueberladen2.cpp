#include<iostream>

void foo(int i)
{
    std::cout <<
    "Ich bin die 'int'-Version.\n";
}

void foo(double d) {
    std::cout <<
    "Ich bin die 'double'-Version.\n";
}

int main() {
{
    foo(3);
    foo(3.0);

    return 0;
}
