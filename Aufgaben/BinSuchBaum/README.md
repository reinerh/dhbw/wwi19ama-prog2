# Aufgaben zu binären Suchbäumen

Verbessern und erweitern Sie den binären Suchbaum aus den Codebeispielen.

Nehmen Sie die dortige Implementierung als Startpunkt und fügen Sie neue Funktionen hinzu bzw. modifizieren Sie die vorhandenen. 

## Verbesserung des In-Order-Durchlaufs

Die Implementierung der Funktion `BinSuchBaum::str()` ist etwas unschön, weil dieser Durchlauf eigentlich durch seine rekursive Natur eher in den `Knoten` gehört.

Erweitern Sie die Klasse `Knoten` um eine Funktion `in_order()`, die einen String mit der In-Order-Darstellung zurückliefert. Rufen Sie diese Funktion anstelle der Implementierung in `BinSuchBaum` auf.

Alternativ: Versuchen Sie sich an einer nicht-rekursiven Version der `str()`-Funktion in `BinSuchBaum`. Achtung, dies ist nicht einfach!

## Pre- und Post-Order-Anzeige, Baum-Darstellung:

Erweitern Sie die Klassen um Funktionen, die den Baum in Pre- und Post-Order-Darstellung anzeigen können.

Darauf aufbauend, fügen Sie eine Funktion hinzu, die eine Baum-Darstellung auf der Konsole anzeigt, z.B. in folgendem Format:

```
A
+--B
|  |
|  +--C
|  |
|  +--D
|
+--E
```

**Hinweis:** Es ist einfacher, wenn Sie die Hilfszeichen `+`, `|` etc. erstmal nicht zeichnen, sondern nur Einrückungen verwenden.
Sobald das funktioniert, können Sie sich um die "Verschönerung" kümmern.

## Aufsuchen von Elementen

Fügen Sie eine Funktion hinzu, die prüft, ob ein bestimmter Wert im Baum enthalten ist.

## Löschen von Elementen

Fügen Sie eine Funktion hinzu, die einen Wert erwartet und die das entsprechende Element aus dem Baum entfernt, sofern es vorhanden ist.

## AVL-Rotationen

Fügen Sie Funktionen hinzu, die die in der Vorlesung besprochenen "L"-, "R"-, "LR"- und "RL"-Rotationen durchführen.

**Hinweis:** Es ist am einfachsten, wenn jede der Funktionen zwei Knoten-Pointer als Parameter erwartet:
Den Knoten, um den rotiert werden soll, und dessen Elternknoten. Im Fall der Wurzel wäre der Elternknoten der Nullpointer.

**Hinweis 2:** Die Doppelrotationen sind sehr einfach, nachdem man die einfachen Rotationen hat. Schreiben Sie sie nicht von Hand, sondern benutzen Sie die Einfachrotationen, um die Doppelrotationen zu implementieren.

Erweitern Sie anschließend die Funktion `add` (und ggf. die Löschfunktion), so dass sie bei Bedarf Rotationen durchführen.

