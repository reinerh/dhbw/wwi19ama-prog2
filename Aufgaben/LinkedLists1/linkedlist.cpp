/** Beschreibung dieser Quelldatei:
 *
 * Diese Datei enthält die Implementierungen für einige der Funktionen,
 * die in linkedlist.h als Member-Funktionen vom LinkedList deklariert sind.
 *
 * Der Header linkedlist.h wird per Include eingebunden, dadurch sind die dortigen
 * Definitionen verfügbar.
 * In dieser Datei werden nur die Implementierungen angegeben. Die Funktionen werden
 * jeweils in der Form TYP KLASSE::FUNKTION angegeben. Der Ausdruck
 *
 * void LinkedList::push_back(int x)
 *
 * besagt z.B., dass jetzt die Funktion void push_back(int x) aus der Klasse LinkedList
 * implementiert wird.
 *
 * Anmerkung: In C++ ist es üblich, dass Klassen in einem Header definiert werden und
 * dass die Implementierungen dann in einer Quelldatei stehen. D.h. i.d.R. hat jede Klasse
 * genau ein Paar aus Header- und Quelldatei.
 * Für diese Aufgaben werden wir ein wenig davon abweichen und zwei Quelldateien verwenden.
 * In dieser Datei sind einige der Funktionen vorgeben, in aufgaben.cpp folgen die restlichen als
 * Vorlagen, die Sie noch ausfüllen sollen.
 * Dies geschieht aus Gründen der besseren Übersicht. Technisch würde nichts dagegen sprechen,
 * die Aufgaben auch hier in dieser Datei zu formulieren.
 */

// Benötigte Includes
#include"linkedlist.h"

#include<iostream>
#include<string>
#include<sstream>

/* Konstruktor
 * Funktion, die bei Erzeugung eines Elements ausgeführt wird.
 * Diese Funktion gibt die Standard-Werte für ein Dummy-Element vor.
 *
 * Zwischen dek Kopf und dem Rumpf der Funktion steht eine sog.
 * Member Initializer List:
 * Nach dem Funktionskopf kommt ein Doppelpunkt und dann eine Aufzählung
 * aller Member (hier data und next) mit dem Wert, den diese bekommen sollen.
 */
Element::Element()
: data(0)
, next(nullptr)
{}

bool Element::isDummy()
{
    return (next == nullptr);
}

LinkedList::LinkedList()
: head(new Element())
{}

void LinkedList::push_back(int x)
{
    // Zeiger, der am Anfang auf den Kopf der Liste zeigt.
    Element * current = head;
    
    // Zeiger verschieben, bis der Dummy (das Element nach dem Ende) gefunden ist.
    while (!current->isDummy())
    {
        current = current->next;
    }
    
    // Daten des Dummys setzen und einen neuen Dummy anhängen.
    current->data = x;
    current->next = new Element();
}

void LinkedList::insert(int pos, int x)
{
    // Zeiger, der am Anfang auf den Kopf der Liste zeigt.
    Element * current = head;
    
    // Zeiger verschieben, bis er auf das pos-te Element zeigt.
    // Wenn die Liste kürzer ist, abbrechen.
    while (!current->isDummy() && pos > 0)
    {
        current = current->next;
        pos--;
    }
    if (current->isDummy() && pos != 0) { return; }
    
    // Neues Element erzeugen und zwischen current und current->next einhängen.
    Element * el = new Element();
    el->data = x;
    el->next = current->next;
    current->next = el;
}

std::string LinkedList::str() const
{
    // Zeiger, der am Anfang auf den Kopf der Liste zeigt.
    Element * current = head;
    
    // Stringstream als Ausgabepuffer
    std::stringstream s;
    
    // Durch die Liste laufen und die Daten jedes Elements an s anhängen.
    while(!current->isDummy())
    {
        s << current->data;
        if (!current->next->isDummy()) { s << " "; }
        current = current->next;
    }
    
    // Den in s gepufferten String zurückgeben
    // Anmerkung: Das hier ist keine Rekursion!
    // Es ist der Aufruf einer Memberfunktion str() von StringStream.
    return s.str();
}

std::ostream & operator<<(std::ostream & left, LinkedList const & right)
{
    return left << right.str();
}

