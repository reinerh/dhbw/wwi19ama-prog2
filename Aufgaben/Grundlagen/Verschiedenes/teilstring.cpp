#include<iostream>
#include<string>
using namespace std;

/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion 'contains', die zwei Strings
    erwartet. Die Funktion soll true zurückliefern, falls der
    erste String im zweiten enthalten ist.
 ***/
bool contains(string s1, string s2);

int main()
{
    cout << contains("ab", "abc") << endl;      // Soll "1" ausgeben (true).
    cout << contains("ab", "acabc") << endl;    // Soll "1" ausgeben (true).
    cout << contains("", "abc") << endl;        // Soll "1" ausgeben (true).
    cout << contains("ab", "acb") << endl;      // Soll "0" ausgeben (false).
    cout << contains("abc", "ab") << endl;      // Soll "0" ausgeben (false).
    
    return 0;
}

