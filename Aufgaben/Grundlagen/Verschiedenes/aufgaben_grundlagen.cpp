/*** BESCHREIBUNG:
    Diese Datei enthält einige einfache Übungsaufgaben zu C++.
    
    Es sollen eine Reihe von Funktionen geschrieben und in der
    Main-Funktion testweise aufgerufen werden. Die Signatur der
    Funktionen ist jeweils vorgegeben.
 ***/

#include<iostream>
#include<string>
#include<vector>
using namespace std;

/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion, die eine Zahl von der Konsole
    einliest und die anschlieﬂend das Doppelte dieser Zahl
    zurückliefert.
 ***/
int zahl_einlesen();


/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion, die so lange Zahlen von der
    Konsole einliest, bis der Benutzer eine 0 eingibt.
    Anschlieﬂend soll die Funktion die Summe der eingegebenen
    Zahlen ausgeben.
 ***/
int summe_einlesen();


/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion, die einen String von der
    Konsole einliest und die diesen String zurückliefert.
 ***/
string string_einlesen();


/*** AUFGABE (Strings/Arrays):
    Schreiben Sie eine Funktion, die einen String als Argument
    erwartet. Die Funktion soll die Anzahl der Buchstaben 'e'
    im String zurückliefern.
 ***/
int anzahl_e(string s);

/*** AUFGABE (Strings/Arrays):
    Schreiben Sie eine Funktion, die einen vector<int>  und
    eine Zahl x als Argumente erwartet.
    Die Funktion soll die Anzahl der Vorkommen von x im Vector
    zurückliefern.
 ***/
int anzahl_x(vector<int> v, int x);


int main()
{
    /** Testen Sie Ihre Funktionen hier in der Main-Funktion **/
    
    return 0;
}
