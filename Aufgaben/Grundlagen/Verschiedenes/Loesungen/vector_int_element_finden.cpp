#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

/* Aufgabenstellung:
 
   Schreiben Sie eine Funktion find(), die einen Vektor v aus
   Zahlen als Parameter sowie einen Wert x erwartet. Die Funktion
   soll das erste Vorkommen von x in v finden und dessen Position
   zurückliefern. Kommt x nicht vor, soll die Funktion die Länge
   von v zurückliefern.
 */
int find(vector<int> v, int x)
{
    for (int i=0; i<v.size(); i++)
    {
        if (v[i] == x)
        {
            return i;
        }
    }
    return v.size();
    
    // Alternative Lösung:
    return std::distance(
      v.begin(),
      std::find(v.begin(), v.end(), x));
}

int main()
{
    std::cout << find({1,3,5,7,9}, 3) << endl; // Soll 1 ausgeben
    std::cout << find({1,3,5,7,9}, 9) << endl; // Soll 4 ausgeben
    std::cout << find({1,3,5,7,9}, 2) << endl; // Soll 5 ausgeben
    std::cout << find({}, 3) << endl;          // Soll 0 ausgeben
    
    return 0;
}
