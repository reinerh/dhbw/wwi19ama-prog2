#include<iostream>
#include<string>
#include<vector>
using namespace std;

/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion 'sorted', die einen String s
    erwartet und die eine sortierte Fassung dieses Strings
    zurückliefert.
 ***/
string sorted(string s);


int main()
{
    string s1 = "hfjgd";
    cout << sorted(s1) << endl;     // soll "dfghj" ausgeben
    
    string s2 = "abba";
    cout << sorted(s2) << endl;     // soll "aabb" ausgeben
    
    string s3 = "";
    cout << sorted(s3) << endl;     // soll "" ausgeben
    return 0;
}

