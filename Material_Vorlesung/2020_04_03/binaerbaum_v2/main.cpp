#include <iostream>
using namespace std;

#include "baum.h"


int main() {
  
  Knoten k;
  cout << k.empty() << endl;   // Es sollte eine 1 ausgegeben werden.

  k.set_data(15);

  cout << k.empty() << endl;   // Es sollte eine 0 ausgegeben werden.
  cout << k.left->empty() << endl;  // 1 erwartet  

  k.left->set_data(3);

  cout << k.data << endl;
  cout << k.left->data << endl;

  return 0;
}
  