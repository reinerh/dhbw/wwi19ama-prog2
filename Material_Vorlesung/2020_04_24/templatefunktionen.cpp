#include <iostream>
using namespace std;

// Allgemeine Template-Definition für Fakultät
template <int N>
int factorial()
{
  return N * factorial<N-1>();
}

// Template-Spezialisierung für N=0
template <>
int factorial<0>()
{
  return 1;
}

int main() {
  std::cout << factorial<5>() << endl;
  return 0;
}
