#include <iostream>
using namespace std;

struct Foo
{
  int i;
};

int main() {
  
  Foo f{42};
  Foo * pf = &f;

  cout << (*pf).i << endl;
  cout << pf->i << endl;


  return 0;
}
