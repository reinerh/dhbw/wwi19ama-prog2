#include <iostream>
using namespace std;

struct Zaehler
{
  int count = 0;

  Zaehler(int startwert) : count(startwert) {}

  // Call- bzw. Aufruf-Operator
  void operator()()
  {
    count++;
  }
};

int main() {
  Zaehler z(5);

  cout << z.count << endl;

  z();
  z();
  z();
  z();

  cout << z.count << endl;

  return 0;
}
