#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <utility>
#include <string>
using namespace std;

int lookup(vector<pair<string,string>> & woerter, string suchwort)
{
  auto iter = find_if(woerter.begin(), woerter.end(),
                       [suchwort](pair<string,string> & el)
                       {
                         return el.first == suchwort;
                       });
  return (iter - woerter.begin());
}


int main() {
  
  vector<int> v1{3,1,15,27,13,42,58,103,38};

  // Definiere eine anonyme Funktion (Lambda)
  //function<bool(int&)> ist_gerade = [](int & el)
  auto ist_gerade = [](int & el)
  {
    return el % 2 == 0;
  };

  vector<int>::iterator iter = find_if(v1.begin(), v1.end(), ist_gerade);

  cout << (iter - v1.begin()) << endl;


  vector<pair<string,string>> woerter {
    {"Affe", "monkey"},
    {"Auto", "car"}
  };
  
  cout << lookup(woerter, "Auto") << endl;

  return 0;
}
