#include <iostream>
#include<vector>
#include<string>
using namespace std;

template<typename T>
T vecsum(vector<T> v)
{
  T result = {}; 
  for (T el:v)
  {
    result = result + el;
  }
  return result;
}





int main() {
  vector<int> v1{ 3, 4, 5 , 7, 13};
  vector<char> v2{ '=', '$', '#'};
  vector<string> v3{ "Hallo", " ", "Welt" };

  cout << vecsum(v1) << endl;
  cout << vecsum(v2) << endl;
  cout << vecsum(v3) << endl;

  return 0;
}