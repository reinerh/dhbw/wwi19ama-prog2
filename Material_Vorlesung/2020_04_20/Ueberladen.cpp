#include <iostream>

int main() {
  std::cout  << (2 << 4) << "\n";
  std::cout  << 2 << 4 << "\n";

  

  std::cout  << 3/4  << " " << 3.0 / 4 << "\n";

  std::cout  << 3+4 << "\n";
  std::cout  << std:: string("abc") + "def" << "\n";
//  std::cout  << "abc" + "def" << "\n"; // Geht nicht, weil "abc" und "def" intern Pointer sind.

  return 0;
}
