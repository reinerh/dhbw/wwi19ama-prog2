#include <iostream>
#include<string>
using namespace std;

template<typename T>
struct List
{
  T data;
  List * next = nullptr;

  void push_back(T x)
  {
    if (next == nullptr)
    {
      
      data = x;
      next = new List();
      return;
    }
    next->push_back(x);
  }
};

int main() {

  List<string> l1;

  l1.push_back("Hallo");

  cout << l1.data << endl;

  return 0;
}