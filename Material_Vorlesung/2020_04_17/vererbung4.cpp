#include <iostream>
#include <fstream>
#include <string>
using namespace std;


class Logger
{
public:
  virtual void write(string) = 0;
};

class ConsoleLogger : public Logger
{
public:
  virtual void write(string) override;
};

class FileLogger : public Logger
{
public:
  virtual void write(string) override;
};

int main() {
  
  FileLogger logger;

  logger.write("Hallo Logger");

  return 0;
}

void ConsoleLogger::write(string msg)
{
  cout << msg << endl;
}

void FileLogger::write(string msg)
{
  fstream outFile("Logdatei.txt");
  outFile << msg << endl;
  outFile.close();
}