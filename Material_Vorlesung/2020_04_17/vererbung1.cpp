#include <iostream>
using namespace std;

class Foo
{
private:
  int x;

public:
  void setX(int);
  int getX();
};

// Bar erbt von Foo (oder Bar spezialisiert Foo)
// D.h. Bar übernimmt alle öffentlichen Funktionen
// Alle öffentlichen Funktionen aus Foo sind auch in Bar öffentlich.
class Bar : public Foo
{
public:
  // Die Funktion setX aus Foo wird überschrieben.
  void setX(int);
};


int main() {
  Foo f;
  f.setX(42);
  cout << f.getX() << endl << endl;

  Bar b;
  b.setX(42);
  cout << b.getX() << endl << endl;
}



void Foo::setX(int x_)
{
  x = x_;
}

int Foo::getX()
{
  return x;
}

void Bar::setX(int x_)
{
  Foo::setX(2 * x_);
}