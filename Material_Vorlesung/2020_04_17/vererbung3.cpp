#include <iostream>
using namespace std;

// Abstrakte Klasse mit Funktionen ohne Implementierung.
class Shape
{
public:
  // Zwei rein virtuelle Funktionen (müssen überschrieben werden).
  virtual float getArea() = 0;
  virtual void scale(int) = 0;
};

class Rectangle : public Shape
{ 
public:
  Rectangle(int width, int height);
  virtual float getArea() override;
  virtual void scale(int) override;

private:
  int height;
  int width;
};

class Triangle : public Shape
{
public:
  Triangle(int base, int height);
  virtual float getArea() override;
  virtual void scale(int) override;

private:
  float base;
  float height;
};


float addAreas(Shape & s1, Shape & s2)
{
  return s1.getArea() + s2.getArea();
}

int main() {
  Rectangle r1(3,4);
  Rectangle r2(10,15);
  Triangle t1(9,5);
  cout << addAreas(r1, t1) << endl;
}


Rectangle::Rectangle(int width_, int height_)
: width(width_)
, height(height_)
{}

float Rectangle::getArea()
{
  return height * width;
}

void Rectangle::scale(int factor)
{
  width *= factor;
}

Triangle::Triangle(int base_, int height_)
: base(base_)
, height(height_)
{}

float Triangle::getArea()
{
  return base * height / 2;
}

void Triangle::scale(int factor)
{
  // TODO
}