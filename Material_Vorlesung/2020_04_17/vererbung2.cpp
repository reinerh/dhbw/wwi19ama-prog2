#include <iostream>
using namespace std;

class Foo
{
private:
  int x;

public:
  virtual void setX(int);
  int getX();
};

// Bar erbt von Foo (oder Bar spezialisiert Foo)
// D.h. Bar übernimmt alle öffentlichen Funktionen
//      Genauer: "Alle öffentlichen Funktionen aus Foo sind auch in Bar öffentlich."
class Bar : public Foo
{
public:
  // Die Funktion setX aus Foo wird überschrieben.
  virtual void setX(int) override;
};


int main() {
  Foo f;
  Bar b;

  Foo & rf = b;

  rf.setX(42);
  cout << rf.getX() << endl << endl;
  
}


void Foo::setX(int x_)
{
  x = x_;
}

int Foo::getX()
{
  return x;
}

void Bar::setX(int x_)
{
  Foo::setX(2 * x_);
}