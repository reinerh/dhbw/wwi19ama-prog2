{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sichtbarkeitsregeln in Klassen"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Include-Anweisungen für die Beispiele aus diesem Notebook\n",
    "#include<string>\n",
    "#include<iostream>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In den bisherigen Beispielen zu \"Schere-Stein-Papier\" haben wir einen Record-Datentyp für den Spieler erzeugt und ihn zu einer Klasse erweitert, indem wir die Funktion `get_move()` zu einer Member-Funktion gemacht haben.\n",
    "Dadurch wird die Idee deutlicher, dass die Entscheidung, wie der Spielzug eines Spielers bestimmt wird,\n",
    "im Verantwortungsbereich des `Player`-Onjekts liegt.\n",
    "\n",
    "Die bisherige Implementierung erlaubt dennoch den freien Zugriff auf den Namen und den Punktestand eines Spielers.\n",
    "Genau genommen liegen auch diese Daten (und die Art, wie sie verändert werden) im Verantwortungsbereich des Spielers.\n",
    "Im Moment wäre es noch möglich, dass z.B. ein Name mitten im Spiel vom Hauptprogramm aus geändert wird, oder\n",
    "dass ein Spieler durch das Hauptprogramm mit einem anderen Punktestand als 0 startet.\n",
    "\n",
    "Das ist in gewisser Weise ein Sicherheitsrisiko. Es ist zwar nicht zu erwarten, dass ein Programmierer solch einen\n",
    "Fehler gezielt einbaut, aber dass er es könnte, ist eine Fehlerquelle.\n",
    "Objektorientierte Sprachen wie C++ bieten einen Mechanismus, um solche Fehlerquellen zu minimieren."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sichtbarkeitsmodifikatoren"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Es ist möglich, für einzelne Klassenmember einzuschränken, wer sie benutzen darf.\n",
    "Ist ein Member als `public` deklariert, so darf er, wie in den bisherigen Beispielen,\n",
    "auch von anderen Objekten außerhalb der Klasse verwendet werden.\n",
    "Ist ein Member hingegen `private`, so darf er nur innerhalb der Implementierung\n",
    "dieser Klasse verwendet werden."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Im `Player`-Beispiel ist es nicht wünschenswert, dass der Name und der Punktestand auf beliebige Weise von außen verändert werden. Die folgende Klassendefinition schränkt die Sichtbarkeit der beiden Attribute `name` und `score` ein, die Funktion `get_move()` hingegen bleibt von außen sichtbar:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```c++\n",
    "class Player {\n",
    "    std::string name;\n",
    "    int score;\n",
    "    \n",
    "public:\n",
    "    int get_move();\n",
    "};\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir haben hier das Schlüsselwort `struct` durch `class` ersetzt.\n",
    "Die beiden Schlüsselwörter unterscheiden sich darin, dass bei `struct` die\n",
    "Sichtbarkeit der Member standardmäßig `public` ist, bei `class` ist sie `private`.\n",
    "Außerdem haben wir oberhalb der Funktion, die zugänglich bleiben soll,\n",
    "die Zeile `public:` eingefügt.\n",
    "Auf diese Weise kann man in C++, die Sichtbarkeiten steuern.\n",
    "Es ist genau so möglich, weiter unten wieder einen `private`-Block zu beginnen usw."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die obige Definition verbietet den Zugriff auf die Attribute `name` und `score`.\n",
    "Ganz so restriktiv wollen wir es aber eigentlich nicht haben, denn zumindest ein\n",
    "Lesezugriff soll ja möglich sein, die Attribute sollen zu Beginn initialisiert werden\n",
    "und `score` sollte erhöht (aber nicht völlig beliebig verändert) werden können.\n",
    "Wir fügen weitere Member-Funktionen hinzu, die diese Dinge ermöglichen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Player {\n",
    "    std::string name;\n",
    "    int score;\n",
    "    \n",
    "public:\n",
    "    // Initialisert ein Spieler-Objekt.\n",
    "    // Sollte genau einmal, direkt nach Konstruktion, aufgerufen werden.\n",
    "    void init(std::string);\n",
    "    \n",
    "    // Liefert den Namen des Spielers.\n",
    "    std::string get_name();\n",
    "    \n",
    "    // Liefert den Punktestand des Spielers.\n",
    "    int get_score();\n",
    "    \n",
    "    // Erhöht den Punktestand des Spielers.\n",
    "    void increase_score();\n",
    "    \n",
    "    // Fragt eine Eingabe vom Spieler ab und liefert\n",
    "    // als Ergebnis 1 (Schere), 2 (Stein) oder 3 (Papier) zurück.\n",
    "    int get_move();\n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mit dieser Klassen-Definition können wir nun arbeiten:\n",
    "Sie lässt die eigentlichen Daten-Member `private` und stellt öffentliche Funktionen\n",
    "bereit, die genau die Änderungen ermöglichen, die für ein `Player`-Objekt erlaubt sein sollen.\n",
    "Diese Technik wird *Kapselung* genannt, die eigentlichen Daten bleiben gewissermaßen verborgen.\n",
    "Die Funktionen bilden die sog. *öffentliche Schnittstelle* der Klasse.\n",
    "Sie sollte gut dokumentiert sein und definiert, wie die Klasse zu benutzen ist.\n",
    "\n",
    "Datenkapselung hat neben einer erhöhten Fehlersicherheit noch einen weiteren Vorteil:\n",
    "Sie ermöglicht es, die interne Darstellung der Daten von der öffentlichen Schnittstelle zu trennen.\n",
    "In der Praxis versucht man zu Beginn der Entwicklung, eine Schnittstelle für seine\n",
    "Klassen zu definieren, die sich nach Möglichkeit später nicht mehr ändert.\n",
    "Andere Entwickler können dann diese Schnittstelle benutzen und sich darauf verlassen,\n",
    "obwohl sich die interne Darstellung möglicherweise von Version zu Version ändert.\n",
    "\n",
    "Dieses Prinzip lässt sich in der Praxis natürlich nicht immer hundertprozentig durchhalten.\n",
    "Dennoch gilt es als Zeichen für schlechtes/fehlerhaftes Design, wenn sich die öffentliche\n",
    "Schnittstelle einer Klasse (oder auch eines ganzen Systems) häufig ändert."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die neuen Funktionen müssen natürlich noch implementiert werden, sie sind glücklicherweise nicht kompliziert:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Player::init(std::string name_)\n",
    "{\n",
    "    name = name_;\n",
    "    score = 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "std::string Player::get_name()\n",
    "{\n",
    "    return name;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int Player::get_score()\n",
    "{\n",
    "    return score;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Player::increase_score()\n",
    "{\n",
    "    score++;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Funktion `get_move()` hat sich gegenüber der Vorversion nicht verändert. Sie darf den Namen des Spielers nach wie vor verwenden, da sie ja selbst ein Klassen-Member ist:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int Player::get_move()\n",
    "{\n",
    "    std::cout << name << \", Ihre moeglichen Zuege sind: \\n\"\n",
    "              << \"1: Schere, 2: Stein, 3: Papier\\n\\n\";\n",
    "    std::cout << \"Bitte geben Sie Ihren Zug ein und bestätigen Sie mit ENTER:\" << std::endl;\n",
    "    \n",
    "    bool valid = false;\n",
    "    std::string input;\n",
    "    while (!valid)\n",
    "    {\n",
    "        std::cin >> input;\n",
    "        valid = (input == \"1\" || input == \"2\" || input == \"3\");\n",
    "        if (!valid)\n",
    "        {\n",
    "            std::cout << \"\\nFehler, bitte versuchen Sie es noch einmal:\" << std::endl;\n",
    "        }\n",
    "    }\n",
    "    \n",
    "    return input[0] - '0';\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Funktion `run()` hat sich durchaus verändert: Statt der Daten-Member von `Player` wird jetzt deren öffentliche Schnittstelle benutzt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void run()\n",
    "{\n",
    "    // Spieler erzeugen.\n",
    "    Player p1, p2;\n",
    "    p1.init(\"Spieler 1\");\n",
    "    p2.init(\"Spieler 2\");    \n",
    "    \n",
    "    // Spiel so lange laufen lassen, bis einer 3 Punkte hat.\n",
    "    while (p1.get_score() < 3 && p2.get_score() < 3)\n",
    "    {\n",
    "        // Spielzuege abfragen\n",
    "        int m1 = p1.get_move();\n",
    "        int m2 = p2.get_move();\n",
    "        \n",
    "        // Auswertung\n",
    "        if (m1 == m2) // Unentschieden\n",
    "        {\n",
    "            std::cout << \"Diese Runde endet unentschieden.\" << std::endl;\n",
    "        }\n",
    "        else if (m2 == m1+1 || m2 == m1-2) // Spieler 2 gewinnt.\n",
    "        {\n",
    "            std::cout << p2.get_name() << \" gewinnt diese Runde.\" << std::endl;\n",
    "            p2.increase_score();\n",
    "        }\n",
    "        else\n",
    "        {\n",
    "            std::cout << p1.get_name() << \" gewinnt diese Runde.\" << std::endl;\n",
    "            p1.increase_score();\n",
    "        }\n",
    "        std::cout << std::endl;\n",
    "    }\n",
    "    \n",
    "    // Ergebnis mitteilen\n",
    "    std::cout << (p1.get_score() == 3 ? p1.get_name() : p2.get_name()) << \" hat gewonnen.\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In den Codebeispielen gibt es auch den [Quellcode zu dieser Version des Spiels](../Codebeispiele/SchereSteinPapier/ssp03.cpp)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "-std=c++17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
