{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Kontrollstrukturen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In diesem Kapitel werden wir die wesentlichen Kontrollstrukturen durchgehen, die es in C++ gibt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Includes für die Beispiele aus diesem Notebook\n",
    "#include<vector>\n",
    "using namespace std;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Schleifen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Schleifen sind Konstrukte, mittels derer sich Anweisungen auf einfache Weise wiederholen lassen.\n",
    "Um bspw. alle Elemente eines `vector<int>` aufzusummieren, definiert man sich eine Hilfsvariable für das (Zwischen-)Ergebnis und addiert dann nacheinander alle Elemente des Vektors auf diese Ergebnisvariable.\n",
    "D.h. es muss eine Aktion, nämlich das Addieren eines Vektor-Elements, mehrfach wiederholt werden.\n",
    "\n",
    "Es gibt in C++ drei verschiedene Arten von Schleifen, mit denen man diese Wiederholung auf unterschiedliche Weise formulieren kann. Dabei liegen drei unterschiedliche Formulierungen der selben Fragestellung zu Grunde:\n",
    "\n",
    "1. \"Wie oft muss die Aktion ausgeführt werden?\"\n",
    "2. \"Für welche Elemente muss die Aktion ausgeführt werden?\"\n",
    "3. \"Unter welcher Bedingung muss die Aktion weiter ausgeführt werden?\"\n",
    "\n",
    "Die drei Formulierungen sind natürlich verwandt, v.A. 1. und 3. sind oft austauschbar.\n",
    "Alle Schleifen bestehen aus zwei Teilen: Einem *Schleifenkopf* und einem *Schleifenrumpf*.\n",
    "Im Schleifenkopf werden die Bedingungen formuliert, unter denen die Schleife laufen soll,\n",
    "im Rumpf stehen dann die Anweisungen, die wiederholt werden sollen, bis die Bedingung aus dem Kopf nicht mehr erfüllt ist.\n",
    "\n",
    "Wir betrachten die drei Schleifen-Typen jetzt im Detail."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### For-Schleife mit Zähler"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dies ist eine klassische Schleife, wie sie in vielen Programmiersprachen vorkommt.\n",
    "Sie entspricht der Fragestellung \"Wie oft muss die Aktion ausgeführt werden?\"\n",
    "\n",
    "Man definiert hier im Schleifenkopf eine Zählvariable, die Bedingung und eine Operation, die nach jedem Durchlauf ausgeführt werden soll. Ein Schleifenkopf, der etwas für alle Elemente eines `vector<int> v` machen soll, würde folgendermaßen aussehen:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```c++\n",
    "for (int i=0; i<v.size(); i++)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dieser Schleifenkopf definiert den Zähler `int i=0`, die Bedingung ist: \"solange i noch nicht die Größe von v erreicht hat\" und nach jedem Schleifendurchlauf soll `i` inkrementiert werden."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Der gesamte Code zum Aufsummieren der Elemente eines Vektors wäre dann:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vector<int> v;\n",
    "int ergebnis;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v = {1,3,5,7,9};\n",
    "ergebnis = 0;\n",
    "for (int i=0; i<v.size(); i++)\n",
    "{\n",
    "    ergebnis = ergebnis + v[i];\n",
    "}\n",
    "ergebnis  // Nicht Teil der Schleife, dient nur zur Anzeige des Ergebnisses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Der Schleifenrumpf steht dabei unter dem Kopf, meist in geschweiften Klammern."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Anmerkung:** Technisch gesehen ist der Schleifenrumpf eine einzige Anweisung.\n",
    "D.h. es ist nicht notwendig, die geschweiften Klammern zu schreiben, wenn (so wie hier) tatsächlich\n",
    "nur eine Anweisung wiederholt werden soll.\n",
    "Mehrere zu wiederholende Anweisungen müssen mit Klammern zu einem Block zusammengefasst werden.\n",
    "Um Fehler zu vermeiden (und der besseren Lesbarkeit halber), ist es ratsam, grundsätzlich die geschweiften Klammern zu verwenden."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Anmerkung:** Die obige Erklärung reflektiert die übliche Verwendung einer For-Schleife mit Zähler.\n",
    "Formal gesehen erlaubt der Kopf mehr als diese eingeschränkte Verwendung: Die Struktur ist generell die folgende:\n",
    "\n",
    "```c++\n",
    "for (<Init-Statement>; <Bedingung>; <Post-Statement>)\n",
    "```\n",
    "\n",
    "Das Init-Statement und das Post-Statement kann jede gültige Anweisung sein, eine Variablendefinition wie oben, ein Funktionsaufruf, generell alles, was sich als einzelne Anweisung schreiben lässt. Die Bedingung muss ein `bool`-Ausdruck sein, also zu `true` oder `false` ausgewertet werden können.\n",
    "Diese Art der For-Schleife ist damit sehr viel ausdrucksstärker als die Verwendung mit Zähler vermuten lässt.\n",
    "Allerdings sollte man, der besseren Lesbarkeit halber, bei komplexen Schleifen eher zur `while`-Schleife (s.u.) greifen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Range Based For-Schleife"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die zweite Formulierung der Fragestellung zu Schleifen war: \"Für welche Elemente muss die Aktion ausgeführt werden?\"\n",
    "Diese Formulierung ist nicht so allgemein verwendbar wie die erste, aber z.B. für Container wie einen `vector<int>` ist sie die klarere Formulierung.\n",
    "Wenn wir über alle Elemente eines Vektors iterieren wollen, ist die Position dieser Elemente oft\n",
    "nicht von Bedeutung. Der Code wird besser lesbar, wenn wir den Schleifenzähler in diesem Fall weglassen.\n",
    "Die obige Schleife sieht als Range-For-Schleife folgendermaßen aus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v = {1,3,5,7,9};\n",
    "ergebnis = 0;\n",
    "for (int el : v)\n",
    "{\n",
    "    ergebnis = ergebnis + el;\n",
    "}\n",
    "ergebnis  // Nicht Teil der Schleife, dient nur zur Anzeige des Ergebnisses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hier wird der Schleifenzähler weggelassen, dafür bekommt jedes Element aus `v`direkt einen Namen, nämlich `el`.\n",
    "Wir können das als \"Für jedes Element `el` aus `v`, ...\" lesen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Eine Range-For-Schleife lässt sich relativ einfach mittels Schleifenzähler simulieren:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v = {1,3,5,7,9};\n",
    "ergebnis = 0;\n",
    "for (int i=0; i<v.size(); i++)\n",
    "{\n",
    "    int el = v[i];\n",
    "    ergebnis = ergebnis + el;\n",
    "}\n",
    "ergebnis  // Nicht Teil der Schleife, dient nur zur Anzeige des Ergebnisses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So gesehen ist die Range-For-Schleife eine abgekürtze Schreibweise für die Verwendung mit Zähler.\n",
    "Sie ist allerdings nicht so implementiert wie in diesem Beispiel, sondern es werden wog. *Iteratoren*\n",
    "dafür verwendet. Dies sind Pointer-Artige Marker, die jeder Container-Typ aus der Standardbibliothek\n",
    "bereitstellt, um auf Elemente zeigen zu können."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Range-For-Schleifen sind immer dann sinnvoll anwendbar, wenn eine Aktion für *alle* Elemente eines Container-Datentyps wie z.B. `vector<int>` ausgeführt werden soll."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Anmerkung:** Dieser Schleifentyp wird in vielen Sprachen als *For-Each* bezeichnet.\n",
    "Er erinnert an die mathematische Sprechweise für Mengen. Bspw. ist die Menge aller geraden Zahlen in mathematischer Notation folgendermaßen definiert:\n",
    "\n",
    "$$2\\mathbb{N} = \\{ x \\in \\mathbb{N} \\mid x = 0 \\mod 2 \\} $$\n",
    "\n",
    "Diese Definition liest man als: \"Die Menge aller natürlichen Zahlen $x$, die durch $2$ teilbar sind\".\n",
    "Der senkrechte Strich in der mathematischen Definition bzw. das Komma im natürlichsprachlichen Satz entspricht in gewisser Weise dem Doppelpunkt in einer Range-For-Schleife."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### While-Schleife"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Der älteste und gleichzeitig allgemeinste Schleifentyp ist die `while`-Schleife.\n",
    "Sie verzichtet auf Init- und Post-Statement und hat nur eine Bedingung im Kopf.\n",
    "Die Vektor-Iteration sieht als While-Schleife folgendermaßen aus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v = {1,3,5,7,9};\n",
    "ergebnis = 0;\n",
    "int i=0;\n",
    "while (i<v.size())\n",
    "{\n",
    "    ergebnis = ergebnis + v[i];\n",
    "    i++;\n",
    "}\n",
    "ergebnis  // Nicht Teil der Schleife, dient nur zur Anzeige des Ergebnisses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Technisch ist die While-Schleife (in C++ und auch in vielen anderen Sprachen) fast äquivalent zur ersten Variante der For-Schleife, Init- und Post-Statement müssen vor dem Kopf bzw. im Schleifenrumpf gemacht werden.\n",
    "Allerdings ist sie eher für allgemeinere Problemstellungen ohne Zähler gedacht. \n",
    "\n",
    "Ein Unterschied in unserem Beispiel zu vorher ist, dass die Zählvariable `i` hier auch nach der Schleife noch gültig ist. Dies ist oft unerwünscht, da man die Namen von Zählvariablen oft wiederverwendet (auch mit unterschiedlichen Typen). Es ist dann unpraktisch, wenn ein einmal definierter Zähler unbegrenzt weiter existiert.\n",
    "Die While-Schleife ist v.A. dann sinnvoll einzusetzen, wenn die notwendigen Voraussetzungen sowieso schon außerhalb hergestellt wurden, so dass bspw. kein separater Zähler mehr notwendig ist."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bedingungen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Neben Schleifen gibt es noch *bedingte Sprünge*, Konstrollstrukturen, die eine Auswahl zwischen zwei Aktionen erlauben oder die eine Aktion nur ausführen, wenn eine bestimmte Bedingung gilt.\n",
    "Das ähnelt ein wenig einer While-Schleife, nur dass die Aktion nur ein einziges Mal ausgeführt wird."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int x;\n",
    "string s;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42;\n",
    "if (x > 23)\n",
    "{\n",
    "    s = \"x ist groesser als 23\";\n",
    "}\n",
    "else\n",
    "{\n",
    "    s = \"x ist kleiner als 23\";\n",
    "}\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Syntax für `if-then-else` ist weitgehend selbsterklärend, in den Klammern muss nur eine Bedingung stehen, die zu `true` oder `false` ausgewertet werden kann. Der `else`-Teil kann auch weggelassen werden, in dam Fall wird die Ausführung des Programms einfach nach dem `if`-Block fortgesetzt, falls die Bedingung nicht erfüllt ist."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Der *ternäre Operator*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Eine Alternative zu `if-then-else`-Anweisungen ist manchmal der sog. *ternäre Operator*.\n",
    "Dabei handelt es sich um einen Operator, der innerhalb anderer Anweisungen genutzt werden kann und der sich\n",
    "ähnlich wie ein `if-then-else` verhällt, allerdings weitaus kürzer ist.\n",
    "Das obige Beispiel mittels des ternären Operators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42;\n",
    "s = (x > 23 ? \"x ist groesser als 23\" : \"x ist kleiner als 23\");\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An `s` wird hier ein Ausdruck zugewiesen, der erst on the fly ausgewertet wird.\n",
    "Vor dem Fragezeichen steht eine boolesche Bedingung. Ist diese Bedingung wahr,\n",
    "so ist das Ergebnis des Ausdrucks der Teil zwischen dem Frageuzeichen und dem\n",
    "Doppelpunkt, ansonsten ist das Ergebnis der Teil nach dem Doppelpunkt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dieses Beispiel ist eine kürzere Variante des obigen `if-then-else`-Beispiels.\n",
    "Der ternäre Operator kann allerdings sehr flexibel wirklich **innerhalb** anderer Anweisungen eingesetzt werden:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42;\n",
    "s = \"x ist \";\n",
    "s += (x > 23 ? \"groesser\" : \"kleiner\");\n",
    "s += \" als 23\";\n",
    "s"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "-std=c++17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
